# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'Bio.intro'
        db.add_column(u'bio_bio', 'intro',
                      self.gf('django.db.models.fields.CharField')(default='test', max_length=500),
                      keep_default=False)

        # Adding field 'Bio.twitter'
        db.add_column(u'bio_bio', 'twitter',
                      self.gf('django.db.models.fields.CharField')(default='test', max_length=200),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'Bio.intro'
        db.delete_column(u'bio_bio', 'intro')

        # Deleting field 'Bio.twitter'
        db.delete_column(u'bio_bio', 'twitter')


    models = {
        u'bio.bio': {
            'Meta': {'object_name': 'Bio'},
            'body': ('django.db.models.fields.TextField', [], {}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '254'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'intro': ('django.db.models.fields.CharField', [], {'default': "'test'", 'max_length': '500'}),
            'phone': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'pub_date': ('django.db.models.fields.DateTimeField', [], {}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'twitter': ('django.db.models.fields.CharField', [], {'default': "'test'", 'max_length': '200'})
        }
    }

    complete_apps = ['bio']