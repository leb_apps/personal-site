from django.contrib import admin
from bio.models import Bio

class BioAdmin(admin.ModelAdmin):
	fields = ['pub_date', 'title', 'intro', 'body', 'email', 'phone', 'twitter']
	list_display = ('title', 'pub_date', 'intro', 'body', 'email', 'phone', 'twitter', 'was_published_recently')

admin.site.register(Bio, BioAdmin)
