from django.conf.urls import patterns, url

from bio import views

urlpatterns = patterns('',
	url(r'^$', views.IndexView.as_view(), name='index'),
	url(r'^login/$', 'django.contrib.auth.views.login', {'template_name': 'bio/login.html'}),
)