import datetime
from django.utils import timezone
from django.db import models

class Bio(models.Model):
	title = models.CharField(max_length = 200)
	intro = models.CharField(max_length = 500, default = 'test')
	body = models.TextField()
	phone = models.CharField(max_length = 200)
	email = models.EmailField(max_length = 254)
	pub_date = models.DateTimeField('date published')
	twitter = models.CharField(max_length = 200, default = 'test')

	def __unicode__(self):
		return self.title

	def was_published_recently(self):
		return self.pub_date >= timezone.now() - datetime.timedelta(days=1)
