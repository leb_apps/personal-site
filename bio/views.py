from django.shortcuts import render
from django.views import generic
from bio.models import Bio

class IndexView(generic.ListView):
    template_name = 'bio/index.html'
    context_object_name = 'latest_record_list'

    def get_queryset(self):
        """Return the last five published records."""
        return Bio.objects.order_by('-pub_date')[:5]